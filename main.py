# configparser
# https://docs.python.org/3/library/configparser.html#configparser.ConfigParser

import configparser, subprocess, os, urllib.request

#Читаем конфигурационный файл config.ini
config = configparser.ConfigParser()
config.read('config.ini')

# Удаляем папку_для_репозитория
def rm_dir_repos():
    if os.path.isdir(config['DEFAULT']['AssemblyFolder']):
        # если это папка, то удаляем её через rmdir
        command_rm_dir_repos = ['rmdir', config['DEFAULT']['AssemblyFolder'], '/S', '/Q']
        exec_rm_dir_repos = subprocess.Popen(command_rm_dir_repos, shell=True, stdout=subprocess.PIPE,
                                             stderr=subprocess.PIPE)
        exec_rm_dir_repos_code = exec_rm_dir_repos.wait()
        if exec_rm_dir_repos_code == 0:
            return exec_rm_dir_repos.wait()
        else:
            print('rm_dir_repos(): Не могу удалить каталог для сборки проекта')
            return exec_rm_dir_repos.wait()
    else:
        # если это файл, то удаляем его через DEL
        command_rm_dir_repos = ['DEL', config['DEFAULT']['AssemblyFolder']]
        exec_rm_dir_repos = subprocess.Popen(command_rm_dir_repos, shell=True, stdout=subprocess.PIPE,
                                             stderr=subprocess.PIPE)
        exec_rm_dir_repos_code = exec_rm_dir_repos.wait()
        if exec_rm_dir_repos_code == 0:
            return exec_rm_dir_repos.wait()
        else:
            print('rm_dir_repos(): Не могу удалить одноимённый с каталогом для сборки проекта файл')
            return exec_rm_dir_repos.wait()

# Копируем репозиторий
def git_clone():
    exec_rm_dir_repos = rm_dir_repos()
    exec_git_clone_code = 1
    if exec_rm_dir_repos == 0:
        # Создаём команду 'git clone репозиторий папка_для_репозитория'
        git_clone = [config['DEFAULT']['PathToGitExec']]
        git_clone.append('clone')
        git_clone.append(config['DEFAULT']['Repository'])
        git_clone.append(config['DEFAULT']['AssemblyFolder'])
        # Выполняем созданную команду для копирования репозитория
        exec_git_clone = subprocess.Popen(git_clone, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        exec_git_clone_code = exec_git_clone.wait()
        # Вывод работы git
        # data = exec_git_clone.communicate()
        # for line in data:
        #     print(line)
        print('git_clone():', exec_git_clone_code)
        return exec_git_clone_code
    else:
        print('git_clone(): Не должно быть папки для репозитория, а она есть')
        return exec_git_clone_code

# Собираем проект
def build_project():
    exec_git_clone_code = git_clone()
    if exec_git_clone_code == 0:
        os.chdir(config['DEFAULT']['AssemblyFolder'])
        name_project = ''
        name_project_file = ''
        for root, dirs, files in os.walk('.'):
            for file in files:
                if file.endswith(".csproj"):
                    name_project_file = os.path.join(root, file)
                    name_project_file = name_project_file[2:]
                    # print('name_project_file:', name_project_file)
                    name_project = name_project_file[:-7]
                    # print('name_project:', name_project)
        # command_build_project = ['dotnet', 'build', '--output', 'exec', file]
        command_build_project = ['dotnet', 'build', name_project_file]
        exec_command_build_project = subprocess.Popen(command_build_project, shell=True,
                                                      stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout = str(exec_command_build_project.stdout.read())
        stdout = stdout[2:-1] # Убираем b и одинарные кавычки по краям
        stdout = stdout.replace('\\\\', '\\') # Убираем двойной обратный слэш
        stdout = stdout[stdout.index('->') + 3:stdout.index('exe') + 3] # Формируем путь до exe'шника
        exec_command_build_project_code = exec_command_build_project.wait()
        print('build_project():', exec_command_build_project_code)
        return exec_command_build_project_code, stdout

# Запускаем бинарник и сохраняем вывод
def exec_bin():
    exec_command_build_project_code, bin_path = build_project()
    if exec_command_build_project_code == 0:
        exec_bin = subprocess.Popen(bin_path, shell=True,
                                                      stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout = str(exec_bin.stdout.read())
        stdout = stdout[2:-1] # Убираем b и одинарные кавычки по краям
        stdout = stdout.replace('\\\\', '\\') # Убираем двойной обратный слэш
        exec_bin_code = exec_bin.wait()
        print('exec_bin():', exec_bin_code)
        return exec_bin_code, stdout
    else:
        exec_bin_code = 1
        return exec_bin_code

def build_package():
    exec_bin_code, stdout_exec_bin = exec_bin()
    # print(exec_bin_code, stdout)
    if exec_bin_code == 0:
        # Скачиваем nuget.exe по адресу из config.ini
        # надо предусмотреть вариант если файл не скачается
        urllib.request.urlretrieve(config['NuGet']['NuGetExeURL'], 'nuget.exe')
        # Создаём NuSpec-файл командой nuget spec (будет создан файл <NameProject>.nuspec)
        command_config_package = ['nuget.exe', 'spec']
        exec_command_config_package = subprocess.Popen(command_config_package, shell=True,
                                                      stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        exec_command_config_package_code = exec_command_config_package.wait()
        # print('exec nuget.exe spec:', exec_command_config_package_code)
        # print('exec nuget.exe spec stdout:', exec_command_config_package.stdout.read())

        # Правим файл <NameProject>.nuspec
        # По правильному дальше нужно работать с xml, но не успел я за 3 часа в этом разобраться,
        # потому будем тупо менять необходимые значения
        config_package_file = ''
        for root, dirs, files in os.walk('.'):
            for file in files:
                if file.endswith(".nuspec"):
                    config_package_file = os.path.join(root, file)
                    config_package_file = config_package_file[2:]
        with open(config_package_file) as file_in:
            text = file_in.read()
        text = text.replace("$id$", config['NuGet']['IDNuGetPackage'])
        text = text.replace("$title$", config['NuGet']['IDNuGetPackage'])
        text = text.replace("$version$", config['NuGet']['VersNuGetPackage'])
        text = text.replace("$author$", config['NuGet']['AuthorNuGetPackage'])
        text = text.replace("$description$", config['NuGet']['DescriptionNuGetPackage'] + ' ' + stdout_exec_bin)
        with open(config_package_file, "w") as file_out:
            file_out.write(text)

        # Собираем NuGet-пакет
        command_build_package = ['nuget.exe', 'pack']
        exec_command_build_package = subprocess.Popen(command_build_package, shell=True,
                                                      stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        exec_command_build_package_code = exec_command_build_package.wait()
        print('build_package():', exec_command_build_package_code)

        # print('exec nuget.exe pack stdout:', exec_command_build_package.stdout.read())

        # # Публикуем NuGet-пакет на nuget.org
        # # и облом, всё совсем не так как тут описано
        # # https://docs.microsoft.com/ru-ru/nuget/quickstart/create-and-publish-a-package-using-the-dotnet-cli
        # name_package_file = ''
        # for root, dirs, files in os.walk('.'):
        #     for file in files:
        #         if file.endswith(".nupkg"):
        #             name_package_file = os.path.join(root, file)
        #             name_package_file = name_package_file[2:]
        # print('name_package_file:', name_package_file)
        # command_push_package = ['nuget.exe', 'push', name_package_file, '-k', config['NuGet']['APIKeyNuGetOrg'],
        #                         '-s', 'https://api.nuget.org/v3/index.json']
        # exec_command_push_package = subprocess.Popen(command_push_package, shell=True,
        #                                               stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        # print('exec nuget.exe push stdout:', exec_command_push_package.stdout.read())

build_package()
